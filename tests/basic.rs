use varint::Varint;

#[test]
fn u8() {
    let v = Varint::from(123_u8);
    assert_eq!(v.as_u64(), 123);
    assert_eq!(v.as_bytes(), [123]);
}

#[test]
fn u16() {
    let v = Varint::from(12345_u16);
    assert_eq!(v.as_u64(), 12345);
    assert_eq!(v.as_bytes(), [253, 48, 57]);
}

#[test]
fn u32() {
    let v = Varint::from(12345678_u32);
    assert_eq!(v.as_u64(), 12345678);
    assert_eq!(v.as_bytes(), [254, 0, 188, 97, 78]);
}

#[test]
fn u64() {
    let v = Varint::from(1234567890123456789_u64);
    assert_eq!(v.as_u64(), 1234567890123456789);
    assert_eq!(v.as_bytes(), [255, 17, 34, 16, 244, 125, 233, 129, 21]);
}
