use varint::Varint;

#[test]
fn compare() {
    let v = Varint::new(1);
    assert_eq!(v, 1);
}

mod add {
    use super::*;

    #[test]
    fn u8() {
        let v = Varint::new(252) + 14_u8;
        assert_eq!(v, 266);
    }

    #[test]
    fn u16() {
        let v = Varint::new(252) + 14234_u16;
        assert_eq!(v, 14486);
    }

    #[test]
    fn u32() {
        let v = Varint::new(252) + 142344545_u32;
        assert_eq!(v, 142344797);
    }

    #[test]
    fn u64() {
        let v = Varint::new(252) + 142344545776_u64;
        assert_eq!(v, 142344546028);
    }

    #[test]
    fn varint() {
        let v = Varint::new(823764023) + Varint::new(12121);
        assert_eq!(v, 823776144);
    }
}
