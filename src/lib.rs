use std::cmp;
use std::ops;

/// `Varint` type encodes integer between 0 and `u64::MAX` as a sequence of 1, 3, 5 or 9 bytes.
///
/// Value in range 0 - 252 (0xFC) occupies one byte (octet) and its value is directly encoded there.
///
///  Value in range 253 (0xFD) - 65535 (0xFFFF) occupies 3 bytes:
///     prefix octet 253 (0xFD),
///     followed by value encoded as two bytes in network byte order (big endian)
///
/// Value in range 65536 (0x1_0000) - 4294967295 (0xFFFF_FFFF) occupies 5 bytes:
///     prefix octet 254 (0xFE),
///     followed by value encoded as four bytes in network byte order
///
/// Value above 4294967296 (0x1_0000_0000) occupies 9 bytes:
///     prefix octet 255 (0xFF),
///     followed by value encoded as eight bytes in network order.
///
#[derive(Debug)]
pub struct Varint;

impl Varint {
    /// # Example
    ///```rust,should_panic
    /// let v = varint::Varint::new(0);
    /// assert_eq!(v.as_bytes(), &[0]);
    ///```
    pub fn new(_value: u64) -> Self {
        Self
    }

    pub fn as_u64(&self) -> u64 {
        todo!()
    }

    pub fn as_bytes(&self) -> &[u8] {
        todo!()
    }
}

impl From<u8> for Varint {
    fn from(_value: u8) -> Self {
        todo!()
    }
}

impl From<u16> for Varint {
    fn from(_value: u16) -> Self {
        todo!()
    }
}

impl From<u32> for Varint {
    fn from(_value: u32) -> Self {
        todo!()
    }
}

impl From<u64> for Varint {
    fn from(_value: u64) -> Self {
        todo!()
    }
}

impl From<Varint> for u64 {
    fn from(_value: Varint) -> Self {
        todo!()
    }
}

impl ops::Add for Varint {
    type Output = Self;

    fn add(self, _rhs: Self) -> Self::Output {
        todo!()
    }
}

impl ops::Add<u8> for Varint {
    type Output = Self;

    fn add(self, _rhs: u8) -> Self::Output {
        todo!()
    }
}

impl ops::Add<u16> for Varint {
    type Output = Self;

    fn add(self, _rhs: u16) -> Self::Output {
        todo!()
    }
}

impl ops::Add<u32> for Varint {
    type Output = Self;

    fn add(self, _rhs: u32) -> Self::Output {
        todo!()
    }
}

impl ops::Add<u64> for Varint {
    type Output = Self;

    fn add(self, _rhs: u64) -> Self::Output {
        todo!()
    }
}

// impl cmp::PartialEq for Varint {
//     fn eq(&self, _other: &Self) -> bool {
//         todo!()
//     }
// }

impl cmp::PartialEq<u64> for Varint {
    fn eq(&self, _other: &u64) -> bool {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic(expected = "not yet implemented")]
    fn it_works() {
        let v = Varint::new(0);
        assert_eq!(v.as_bytes(), &[0]);
    }
}
